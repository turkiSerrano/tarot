package customWidget;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable.Orientation;
import android.opengl.Visibility;
import android.text.InputType;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Switch;
import android.widget.TextView;

import com.example.tarot.R;

public class WidgetFactory {

	private final static int paddingTopScore = 200;
	private final static int paddingEditNText = 20;
	private final static int paddingWidgetScore = 40;
	
	@SuppressLint("NewApi")
	public static View creerHorizontalWidget(Context context,Resources resources) 
	{
		LinearLayout widgetLayout = new LinearLayout((context));
		widgetLayout.setPadding( 0, 80, 0, 0);
		widgetLayout.setGravity(Gravity.CENTER);
		
		//saisie nom joueur
		AutoCompleteExtended autoCompleteTextView = new AutoCompleteExtended(context);
		autoCompleteTextView.setPadding(5, 0, 0, 0);
		autoCompleteTextView.setWidth(250);
		autoCompleteTextView.setHint(R.string.widget_choixJoueur);
		autoCompleteTextView.setTextColor(Color.BLACK);
		
		//seek bar
		Switch choixEquipe = new Switch(context);
		choixEquipe.setPadding(40, 0, 0, 0);
		choixEquipe.setLayoutParams(new LayoutParams( 200, LayoutParams.WRAP_CONTENT));
		
		TextView scoreJoueur = new TextView(context);
		scoreJoueur.setText("essai");
		scoreJoueur.setPadding(20, 0, 0, 0);
		scoreJoueur.setVisibility(View.INVISIBLE);
		
		//ajout layout
		widgetLayout.addView(autoCompleteTextView);
		widgetLayout.addView(choixEquipe);
		widgetLayout.addView(scoreJoueur);
		return widgetLayout;
	}

	

	@SuppressLint("InlinedApi")
	public static ViewGroup creerVerticalLayoutExtended(int widgetsACreer,Context context,
			Resources resources)
	{
				
		//header view group
		LinearLayout layoutExtended = new LinearLayout(context);
		layoutExtended.setOrientation(LinearLayout.VERTICAL);
		layoutExtended.setPadding(0, 50, 0, 0);
		for (int i = 0; i < widgetsACreer; i++) {
			layoutExtended.addView(creerHorizontalWidget(context,resources));
		}
		
		//ajout zone saisie score, layout score
		LinearLayout layoutScore = new LinearLayout(context);
		layoutScore.setPadding(0, paddingTopScore, 0, 0);
		layoutScore.setGravity(Gravity.CENTER);
		
		//1 er doublons text et edit
		TextView textViewPointPreneur = new TextView(context);
		textViewPointPreneur.setTextColor(Color.BLACK);
		textViewPointPreneur.setText(resources.getString(R.string.points_preneur));
		textViewPointPreneur.setPadding(paddingWidgetScore, 0, paddingEditNText, 0);
		
		EditTextExtended pointPreneur = new EditTextExtended(context);
		pointPreneur.setInputType(InputType.TYPE_CLASS_NUMBER);
		pointPreneur.setWidth(100);
		pointPreneur.setTextColor(Color.BLACK);
		pointPreneur.setPadding(paddingEditNText, 0, 0, 0);
		
		
		//2 eme doublons text et edit
		TextView textViewPointRestant = new TextView(context);
		textViewPointRestant.setPadding(paddingWidgetScore, 0, paddingEditNText, 0);
		textViewPointRestant.setTextColor(Color.BLACK);
		textViewPointRestant.setText(resources.getString(R.string.points_restant));
		
		EditTextExtended pointRestant = new EditTextExtended(context);
		pointRestant.setInputType(InputType.TYPE_CLASS_NUMBER);
		pointRestant.setWidth(100);
		pointRestant.setTextColor(Color.BLACK);
		pointRestant.setPadding(paddingEditNText, paddingEditNText, 0, 0);

		Button submit = new Button(context);
		submit.setLayoutParams(new LayoutParams(ActionBar.LayoutParams.WRAP_CONTENT,ActionBar.LayoutParams.WRAP_CONTENT));
		submit.setText(resources.getString(R.string.bt_submit));
		
		//ajout layout score
		layoutScore.addView(textViewPointPreneur);
		layoutScore.addView(pointPreneur);
		layoutScore.addView(textViewPointRestant);
		layoutScore.addView(pointRestant);
		layoutScore.addView(submit);
		
		//ajout au layout final
		layoutExtended.addView(layoutScore);
		layoutExtended.addView(creerLayoutOptions(context, resources));
		
		return layoutExtended; 
	}
	
	@SuppressLint("NewApi")
	public static LinearLayout creerLayoutScore(Context context,Resources resources)
	{
		
		LinearLayout linearLayout = new LinearLayout(context);
		linearLayout.setPadding(0, 50, 0, 0);
		linearLayout.setGravity(Gravity.CENTER);
		
		//textview
		TextView scorePreneur = new TextView(context);
		scorePreneur.setHeight(80);
		scorePreneur.setWidth(150);
		scorePreneur.setGravity(Gravity.CENTER);
		scorePreneur.setTextSize(35);
		scorePreneur.setTextColor(Color.BLACK);
		
		TextView textView = new TextView(context);
		textView.setWidth(150);
	
		TextView scoreRestant = new TextView(context);
		scoreRestant.setHeight(80);
		scoreRestant.setWidth(150);
		scoreRestant.setGravity(Gravity.CENTER);
		scoreRestant.setTextSize(35);
		scoreRestant.setTextColor(Color.BLACK);
		
		//ajout
		linearLayout.addView(scorePreneur);
		linearLayout.addView(textView);
		linearLayout.addView(scoreRestant);
		return linearLayout;
	}
	
	public static LinearLayout creerLayoutOptions(Context context,Resources resources)
	{
		LinearLayout layoutOptions = new LinearLayout(context);
		RadioGroup contrats = new RadioGroup(context);
		contrats.setOrientation(RadioGroup.HORIZONTAL);
		
		RadioButton petite = new RadioButton(context);
		petite.setText("petite");
		contrats.addView(petite);
		
		layoutOptions.addView(contrats);
		return layoutOptions;
		
	}

}
