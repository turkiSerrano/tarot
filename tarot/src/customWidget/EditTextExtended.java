package customWidget;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.widget.EditText;

public class EditTextExtended extends EditText{

	private  Rect rect;
	private Paint paint;
	
	public EditTextExtended(Context context) {
		super(context);
		rect = new Rect();
		paint = new Paint();
	}
	
	public void onDraw(Canvas canvas)
	{
		super.onDraw(canvas);
        paint.setStyle(Paint.Style.STROKE);
        paint.setColor(Color.LTGRAY);
        paint.setStrokeWidth(3);
        getLocalVisibleRect(rect);
        canvas.drawRect(rect, paint); 
	}

}
