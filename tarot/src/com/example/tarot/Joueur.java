package com.example.tarot;

public class Joueur {
	
	private String nomJoueur;
	private long id;
	private int nbrPartiesGagnées;

	public String getNomJoueur() {
		return nomJoueur;
	}

	public void setNomJoueur(String nomJoueur) {
		this.nomJoueur = nomJoueur;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

}
