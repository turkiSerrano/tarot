package com.example.tarot;

public class Partie {
	
	Joueur[] preneurs;
	Joueur[] restants;
	private long id;
	private int pointPreneurs;
	private int pointRestants;
	private int nombreBouts;
	private int contrat;
	
	
	Partie(Joueur[] preneurs, Joueur[] restants)
	{
		this.preneurs = preneurs;
		this.restants = restants;
	}
	
	public Partie()
	{
		this.preneurs = null;
		this.restants = null;
	}

	public void setId(long long1) {
		// TODO Auto-generated method stub
		this.id = long1;
	}

	public int getPointPreneurs() {
		return pointPreneurs;
	}

	public void setPointPreneurs(int pointPreneurs) {
		this.pointPreneurs = pointPreneurs;
	}

	public void setPointRestants(int pointRestants)
	{
		this.pointRestants = pointRestants;
	}
	
	public int getPointRestants() {
		return pointRestants;
	}

	public int getNombreBouts() {
		return nombreBouts;
	}

	public void setNombreBouts(int nombreBouts) {
		this.nombreBouts = nombreBouts;
		
	}

	

}
