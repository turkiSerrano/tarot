package activity;

import java.util.HashMap;
import java.util.List;

import util.BadValueException;
import android.animation.LayoutTransition;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.tarot.Joueur;
import com.example.tarot.Partie;
import com.example.tarot.R;

import customWidget.AutoCompleteExtended;
import customWidget.WidgetFactory;
import database.DAOJoueur;
import database.DAOPartie;


public class ChoixJoueurs extends Activity implements OnClickListener{

	private ViewGroup vuesChoixJoueur;
	private ViewGroup zoneScore;
	private ViewGroup score;
	private DAOJoueur daoJoueur;
	private DAOPartie daoPartie;
	private int limite;
	private final int positionAutoCompleteList = 0;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_choix_joueurs);
		
		Intent intent = getIntent();
		limite = intent.getIntExtra(MainActivity.MESSAGE_INTENT, 0);
		Log.d("Choix Joueurs","limite joueurs :"+limite);
		
		//initialisation layout
		vuesChoixJoueur = WidgetFactory.creerVerticalLayoutExtended(limite, 
				getApplicationContext(), getResources());
		LinearLayout layout = WidgetFactory.creerLayoutOptions(getApplicationContext(),getResources());
		vuesChoixJoueur.addView(layout);
		zoneScore = (LinearLayout) vuesChoixJoueur.getChildAt(limite);
		
		//add listener
		Button button =  (Button) zoneScore.getChildAt(zoneScore.getChildCount()-1);
		button.setOnClickListener(this);
		
		//initialize DAO
		Log.d("ChoixParties"," avant appel open");
		daoPartie = new DAOPartie(this);
		daoJoueur = new DAOJoueur(this);
		daoJoueur.open();
		daoPartie.open();
		initializeAutoCompleteListe(vuesChoixJoueur); 
		
		//modify view
		setContentView(vuesChoixJoueur);
	}

	private void initializeAutoCompleteListe(ViewGroup group) {
		List<Joueur> nomJoueurs = daoJoueur.getAllJoueur();
		String[] tableauJoueurs = new String[nomJoueurs.size()];
		int i = 0;
		for (Joueur joueur : nomJoueurs) {
			tableauJoueurs[i] = joueur.getNomJoueur();
			i++;
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,android.R.layout.simple_dropdown_item_1line,tableauJoueurs);
		for(int j=0;j<3;j++)
		{
			ViewGroup sousGroupe = (ViewGroup) group.getChildAt(j);
			AutoCompleteExtended autoCompletToModify = (AutoCompleteExtended) sousGroupe.getChildAt(0);
			autoCompletToModify.setThreshold(1);
			autoCompletToModify.setAdapter(adapter);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu)
	{
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.choix_joueurs, menu);
		return true;
	}

	
	@Override
	protected void onPause() {
		daoJoueur.close();
		super.onPause();
	}
	
	@Override
	public void onResume()
	{
		daoJoueur.open();
		super.onResume();
	}
	
	@Override
	/***
	 * fonction qui sauvegarde dans la base les joueurs et la partie concern�es
	 */
	public void onClick(View v) {
		
		Log.d("Choix Joueurs","nombre de joueurs "+vuesChoixJoueur.getChildCount());
		Partie partie = new Partie();
		HashMap<String, Joueur> liste = daoJoueur.getTableAllJoueurs();
		
		//creation des obj joueurs, insertion base de donn�es
		for(int i=0; i<3;i++)
		{
			ViewGroup view = (ViewGroup) vuesChoixJoueur.getChildAt(i);
			View sousView = view.getChildAt(this.positionAutoCompleteList);
			if(sousView instanceof AutoCompleteTextView)
			{
				AutoCompleteTextView autoCompleteTextView = (AutoCompleteTextView) sousView;
				Log.d("Choix Joueurs","View nom joueur "+autoCompleteTextView.getText());
				if(!liste.containsKey(autoCompleteTextView.getText().toString()))
				{
					Joueur joueur = daoJoueur.createJoueur(autoCompleteTextView.getText().toString());
					Log.d("Choix joueur","POJO joueur "+joueur.getNomJoueur());
				}
			}
			else
			{
				Log.d("ChoixJoueurs","probleme db");
			}
		}
		
		try 
		{
			//calcule les point de chaque joueur

			if(score == null)
			{
				score = WidgetFactory.creerLayoutScore(getApplicationContext(), getResources());
				this.miseAJourPoint(partie);
				vuesChoixJoueur.addView(score);
				
			}
			else
			{
				this.miseAJourPoint(partie);
			}
			//boucle pour afficher les score individuels
			for(int i=0; i< limite; i++) 
			{
				LinearLayout layout = (LinearLayout) vuesChoixJoueur.getChildAt(i);
				TextView textView = (TextView) layout.getChildAt(layout.getChildCount()-1);
				textView.setText("on click");
				textView.setVisibility(View.VISIBLE);
			}
			
		} catch (BadValueException e) {
			//TODO message ou autre
		}
	}
	
	private void miseAJourPoint(Partie partie) throws BadValueException
	{
		EditText pointsPreneurs = (EditText) zoneScore.getChildAt(1);
		EditText pointRestants = (EditText) zoneScore.getChildAt(3);
		
		//initialisation
		TextView scoreRestant = (TextView) score.getChildAt(2);
		TextView scorePreneur = (TextView) score.getChildAt(0);
		
		String tmpPointPreneur = pointsPreneurs.getText().toString(); 
		String tmpPointRestant = pointRestants.getText().toString();

		if(tmpPointPreneur.length() > 0)
		{
			partie.setPointPreneurs(Integer.parseInt(tmpPointPreneur));
			partie.setPointRestants(91 - partie.getPointPreneurs());
		}
		else
		{
			partie.setPointRestants(Integer.parseInt(tmpPointRestant));
			partie.setPointPreneurs(91 - partie.getPointRestants());
			
		}
		
		if(partie.getPointPreneurs() > partie.getPointRestants())
		{	
			scoreRestant.setBackgroundColor(Color.RED);
			scorePreneur.setBackgroundColor(Color.GREEN);
		}
		else
		{
			scoreRestant.setBackgroundColor(Color.GREEN);
			scorePreneur.setBackgroundColor(Color.RED);
		}
		
		daoPartie.createPartie(partie.getPointPreneurs(), partie.getPointRestants());
		scoreRestant.setText(String.valueOf(partie.getPointRestants()));
		scorePreneur.setText(String.valueOf(partie.getPointPreneurs()));
	}

}
