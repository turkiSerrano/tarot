package activity;

import com.example.tarot.R;
import database.DAOJoueur;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

public class MainActivity extends Activity implements OnItemSelectedListener, OnClickListener{

	private int nombre_joueurs;
	private int nombre_joueurs_defaut;
	private DAOJoueur daoJoueur;
	static String MESSAGE_INTENT = "nombre_joueurs"; 
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);		
		//ajout liste des joueurs possible dans le spinner
		Spinner spinner = (Spinner) findViewById(R.id.spinner1);
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, 
				R.array.liste_nombres_joueurs_possible, android.R.layout.simple_spinner_item);
		adapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
		spinner.setAdapter(adapter);
		spinner.setOnItemSelectedListener(this);
		daoJoueur = new DAOJoueur(this);
		daoJoueur.open();
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public void onItemSelected(AdapterView<?> arg0, View arg1, int arg2,
			long arg3) {
		this.setNombre_joueurs(Integer.parseInt( (String) arg0.getItemAtPosition(arg2)));
		Log.d("Main Activity","nombre joueurs :"+this.nombre_joueurs);
		
	}

	@Override
	public void onNothingSelected(AdapterView<?> arg0) {
		this.setNombre_joueurs(4);
		Log.d("Main Activity","nombre joueurs (defaut):"+this.nombre_joueurs);
	}
	
	public void InitActiviteChoixJoueurs(View view)
	{
		Intent intent = new  Intent(this, ChoixJoueurs.class);
		intent.putExtra(MESSAGE_INTENT, this.nombre_joueurs);
		startActivity(intent);
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		
	}

	//accesseurs
	public int getNombre_joueurs() {
		return nombre_joueurs;
	}

	public void setNombre_joueurs(int nombre_joueurs) {
		this.nombre_joueurs = nombre_joueurs;
	}

	public int getNombre_joueurs_defaut() {
		return nombre_joueurs_defaut;
	}

	public void setNombre_joueurs_defaut(int nombre_joueurs_defaut) {
		this.nombre_joueurs_defaut = nombre_joueurs_defaut;
	}


}
