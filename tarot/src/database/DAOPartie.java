package database;

import java.util.ArrayList;
import java.util.List;

import com.example.tarot.Joueur;
import com.example.tarot.Partie;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class DAOPartie {
	private SQLiteDatabase database;
	private SQLiteHelper sqLiteHelper;
	private String[] allColumns = {SQLiteHelper.COLUMN_ID, SQLiteHelper.COLUMN_POINT_PRENEUR, SQLiteHelper.COLUMN_POINT_RESTANT};
	
			
	public DAOPartie(Context context)
	{
		sqLiteHelper = new SQLiteHelper(context);
	}
	
	public void open() throws SQLException
	{
			database = sqLiteHelper.getWritableDatabase();
	}

	public void close()
	{
		sqLiteHelper.close();
	}
	
	public Partie createPartie(int pointPreneurs, int pointRestants) 
	{
	    ContentValues values = new ContentValues();
	    values.put(SQLiteHelper.COLUMN_POINT_PRENEUR, pointPreneurs);
	    values.put(SQLiteHelper.COLUMN_POINT_RESTANT, pointRestants);
	    long insertId = database.insert(SQLiteHelper.TABLE_PARTIES, null,
	        values);
	    Cursor cursor = database.query(SQLiteHelper.TABLE_PARTIES,
	        allColumns, SQLiteHelper.COLUMN_ID + " = '" + insertId+"'", null,
	        null, null, null);
	    cursor.moveToFirst();
	    Partie newComment = cursorToPartie(cursor);
	    cursor.close();
	    return newComment;
	}
	

	public List<Partie> getAllParties() {
	    List<Partie> joueurs = new ArrayList<Partie>();

	    Cursor cursor = database.query(SQLiteHelper.TABLE_JOUEURS,
	        allColumns, null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Partie joueur = cursorToPartie(cursor);
	      Log.d("DAOPartie "," partie pts preneurs : "+joueur.getPointPreneurs()+" "+"partie pts restants "+joueur.getPointRestants());
	      joueurs.add(joueur);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return joueurs;
	  }
	
	
	public void updateJoueur(Partie partie)
	{
		
	}
	
	private Partie cursorToPartie(Cursor cursor)
	{
		Partie partie = new Partie();
		partie.setId(cursor.getLong(0));
		partie.setPointPreneurs(cursor.getInt(1));
		partie.setPointRestants(cursor.getInt(2));
		return partie;
	}
}
