package database;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.tarot.Joueur;

public class DAOJoueur {
	
	private SQLiteDatabase database;
	private SQLiteHelper sqLiteHelper;
	private String[] allColumns = {SQLiteHelper.COLUMN_ID, SQLiteHelper.COLUMN_NOM};
	
	public DAOJoueur(Context context)
	{
		sqLiteHelper = new SQLiteHelper(context);
	}
	
	public void open() throws SQLException
	{
		database = sqLiteHelper.getWritableDatabase();
	}

	public void close()
	{
		sqLiteHelper.close();
	}
	
	public Joueur createJoueur(String nomJoueur) 
	{
	    ContentValues values = new ContentValues();
	    values.put(SQLiteHelper.COLUMN_NOM, nomJoueur);
	    long insertId = database.insert(SQLiteHelper.TABLE_JOUEURS, null,
	        values);
	    Cursor cursor = database.query(SQLiteHelper.TABLE_JOUEURS,
	        allColumns, SQLiteHelper.COLUMN_ID + " = " + insertId, null,
	        null, null, null);
	    cursor.moveToFirst();
	    Joueur newComment = cursorTojoueur(cursor);
	    cursor.close();
	    return newComment;
	}
	

	public List<Joueur> getAllJoueur() {
	    List<Joueur> joueurs = new ArrayList<Joueur>();

	    Cursor cursor = database.query(SQLiteHelper.TABLE_JOUEURS,
	        allColumns, null, null, null, null, null);

	    cursor.moveToFirst();
	    while (!cursor.isAfterLast()) {
	      Joueur joueur = cursorTojoueur(cursor);
	      Log.d("DAOJoueur","joueur "+joueur.getNomJoueur());
	      joueurs.add(joueur);
	      cursor.moveToNext();
	    }
	    // Make sure to close the cursor
	    cursor.close();
	    return joueurs;
	}
	
	public HashMap<String, Joueur> getTableAllJoueurs()
	{
		HashMap<String, Joueur> liste = new HashMap<String, Joueur>();
		Cursor cursor = database.query(SQLiteHelper.TABLE_JOUEURS,
		        allColumns, null, null, null, null, null);

		    cursor.moveToFirst();
		    while (!cursor.isAfterLast()) {
		      Joueur joueur = cursorTojoueur(cursor);
		      Log.d("DAOJoueur","joueur "+joueur.getNomJoueur());
		      liste.put(joueur.getNomJoueur(),joueur);
		      cursor.moveToNext();
		    }
		return liste;
	}
	
	
	public void updateJoueur(Joueur joueur)
	{
		
	}
	
	private Joueur cursorTojoueur(Cursor cursor)
	{
		Joueur joueur = new Joueur();
		joueur.setId(cursor.getLong(0));
		joueur.setNomJoueur(cursor.getString(1));
		return joueur;
	}
	
}
