package database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper{

		//tables
	  public static final String TABLE_JOUEURS = "joueurs";
	  public static final String TABLE_PARTIES = "parties";
	  
	  //column
	  public static final String COLUMN_ID = "_id";
	  public static final String COLUMN_NOM = "nom_joueur";
	  public static final String COLUMN_POINT_PRENEUR = "point_preneur";
	  public static final String COLUMN_POINT_RESTANT = "point_restants";
	  
	  private static final String DATABASE_NAME = "joueurs.db";
	  private static final int DATABASE_VERSION = 1;
	  
	 // Database creation sql statement
	  private static final String CREATE_TABLE_JOUEUR_ = "create table "
	      + TABLE_JOUEURS + "(" + COLUMN_ID
	      + " integer primary key autoincrement, " + COLUMN_NOM
	      + " text not null);";
	  
	  public static final String CREATE_TABLE_PARTIES = "create table "
		      + TABLE_PARTIES + "(" + COLUMN_ID
		      + " integer primary key autoincrement, " 
		      + COLUMN_POINT_PRENEUR 
		      + " integer not null,"
		      + COLUMN_POINT_RESTANT
		      + " integer not null);";
	  
	public SQLiteHelper(Context context) 
	{
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.d("SQLiteHelper", "debut fct creation table");
		db.execSQL(CREATE_TABLE_JOUEUR_);
		db.execSQL(CREATE_TABLE_PARTIES);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
	}

}
